package com.br.exemplo.testng.service.email;

public interface EmailGenerator {
    String generate();
}
