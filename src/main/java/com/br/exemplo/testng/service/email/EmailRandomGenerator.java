package com.br.exemplo.testng.service.email;


import org.springframework.stereotype.Service;

@Service
public class EmailRandomGenerator implements EmailGenerator {

    @Override
    public String generate(){
        return "alexsandro@teste.com.br";
    }
}
