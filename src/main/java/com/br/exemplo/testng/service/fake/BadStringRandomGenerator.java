package com.br.exemplo.testng.service.fake;

import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Random;

@Service
public class BadStringRandomGenerator implements BadStringGenarate {

    @Override
    public String generate() {
        return getLinesFile();
    }

    private String getLinesFile() {
        List<String> content = null;
        try {
            content = Files.readAllLines(getFile().toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Random rand = new Random();
        int i = rand.nextInt(content.size());
        System.out.println("Linha impressa " + i);

        return content.get(i);
    }

    private File getFile() {
        File file = null;
        try {
            file = ResourceUtils.getFile("classpath:big_list_of_naughty_strings.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return file;
    }
}
