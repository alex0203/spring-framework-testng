package com.br.testng;

import com.br.exemplo.testng.service.email.EmailGenerator;
import com.br.exemplo.testng.service.fake.BadStringGenarate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNotNull;


@TestPropertySource("/alex.properties")
@Test
@ContextConfiguration(locations = { "classpath:spring-test-config.xml" })
public class ExemploTest extends AbstractTestNGSpringContextTests {

    @Value("${email}")
    private String emailFromProperties;

    @Autowired
    EmailGenerator emailGenerator;

    @Autowired
    BadStringGenarate badString;

    @Test()
    void testEmailGenerator() {
        String email = emailGenerator.generate();

        assertNotNull(email);
        assertEquals(email, emailFromProperties);

    }
    @Test()
    void testStringGenerator() {
        String badStringUse = badString.generate();
        System.out.print(badStringUse);
        assertNotNull(badStringUse);

    }

}